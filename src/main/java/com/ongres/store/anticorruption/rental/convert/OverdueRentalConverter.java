package com.ongres.store.anticorruption.rental.convert;

import com.ongres.store.application.api.reports.rental.OverdueRentalReport;
import com.ongres.store.domain.rental.Rental;

import java.util.List;

public interface OverdueRentalConverter {
    List<OverdueRentalReport> from(List<Rental> rentals);
}
