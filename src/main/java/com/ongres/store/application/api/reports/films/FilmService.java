package com.ongres.store.application.api.reports.films;

import com.ongres.store.domain.film.Film;

import java.util.List;

public interface FilmService {
    List<Film> searchByActor(String firstName, String lastName);
}
