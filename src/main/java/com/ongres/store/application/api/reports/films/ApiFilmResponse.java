package com.ongres.store.application.api.reports.films;

import com.ongres.store.application.api.reports.customer.ApiContentResponse;
import com.ongres.store.domain.film.Film;

import java.util.List;

public class ApiFilmResponse extends ApiContentResponse {

    public ApiFilmResponse(List<Film> films) {
        super(films);
    }

}
