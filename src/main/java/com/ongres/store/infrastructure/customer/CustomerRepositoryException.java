package com.ongres.store.infrastructure.customer;

import com.ongres.store.infrastructure.exception.StoreException;

public class CustomerRepositoryException extends StoreException {

    public CustomerRepositoryException(String message) {
        super(message);
    }
}
