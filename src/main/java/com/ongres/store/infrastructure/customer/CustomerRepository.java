package com.ongres.store.infrastructure.customer;

import com.ongres.store.infrastructure.ConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.ongres.store.infrastructure.customer.CustomerMappedQuery.CUSTOMER_BY_COUNTRY;
import static com.ongres.store.infrastructure.customer.CustomerMappedQuery.CUSTOMER_BY_COUNTRY_AND_CITY;
import static java.lang.String.format;

@Repository
public class CustomerRepository implements CustomerDao {

    @Autowired
    private ConnectionManager connectionManager;

    @Override
    public Long totalByCountry(String country) {
        try {
            Object[] filter = new Object[] { country.toLowerCase() };
            //TODO
            Long totalCustomers = (Long) connectionManager.queryObject(CUSTOMER_BY_COUNTRY.getRawValue(),filter,Long.class);
            return totalCustomers;
        } catch (Throwable exception){
            exception.printStackTrace();
        }
        //TODO
        throw new CustomerRepositoryException(format("It was not possible calculate total customer by country. Country %s",country));
    }

    @Override
    public Long totalByCountryAndCity(String country, String city) {
        try {
            Object[] filter = new Object[] { country.toLowerCase(), city.toLowerCase() };
            //TODO
            Long totalCustomers = (Long) connectionManager.queryObject(CUSTOMER_BY_COUNTRY_AND_CITY.getRawValue(),filter,Long.class);
            return totalCustomers;
        } catch (Throwable exception){
            exception.printStackTrace();
        }
        //TODO
        throw new CustomerRepositoryException(format("It was not possible calculate total customer by country ad city. Country %s / City %s",country, city));
    }
}
