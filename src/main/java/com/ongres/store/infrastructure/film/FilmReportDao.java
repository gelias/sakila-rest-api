package com.ongres.store.infrastructure.film;

import com.ongres.store.domain.film.Film;

import java.util.List;

public interface FilmReportDao {
    List<Film> searchByActor(String firstName, String lastName);
}
