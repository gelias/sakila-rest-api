package com.ongres.store.infrastructure.rental;

import com.ongres.store.domain.rental.Rental;

import java.util.List;

public interface RentalDao {
    List<Rental> overdueRentalsByDate();
}
