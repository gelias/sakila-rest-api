package com.ongres.store.infrastructure.rental;

import com.ongres.store.domain.rental.Rental;
import com.ongres.store.infrastructure.ConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.ongres.store.infrastructure.rental.RentalMappedQuery.RENTAL_OVERDUE;

@Component
public class RentalRepository implements RentalDao {

    @Autowired
    private ConnectionManager connectionManager;

    @Override
    public List<Rental> overdueRentalsByDate() {
        return connectionManager.getConnection().query(RENTAL_OVERDUE.getRawValue()
                ,(rs, rowNum) -> new Rental(
                        rs.getString("customer"),
                        rs.getString("phone"),
                        rs.getString("title")
                ));
    }
}
